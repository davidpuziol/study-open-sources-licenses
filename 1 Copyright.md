# Copyright (Direitos Autorais)?

![copyright](./pics/copyright-symbol-svgrepo-com.svg)

Copyright é um direito legal que protege as obras criativas, como livros, músicas, filmes, pinturas, fotografias, `softwares`, etc de serem copiadas, distribuídas, exibidas ou reproduzidas sem permissão do seu criador. Ele concede ao criador ou proprietário do trabalho o direito exclusivo de controlar a forma como a obra é utilizada e como outras pessoas podem usar ou reproduzir seu trabalho. O objetivo do copyright é incentivar a criação de novas obras e proteger o direito dos criadores de controlar e se beneficiar de suas criações.

Os direitos autorais são dados ao autor automaticamente mesmo sem uma aprovação governamental ou um documento, desde que o criador prove a que obra é de sua autoria. Porém, sem um documento explícito (licensa) definindo a forma com que o autor quer distribuir seu trabalho, nada é permitido.

Softwares são geralmente protegidos por direitos autorais, mas um algoritmo não pode ser protegido. Algoritmo não é considerado criativo, MAS uma forma específica de expressão de um algoritmo pode.

O trabalho precisa ser original, não copiado, mas não necessariamente novo. Por exemplo, duas pessoas podem bater foto do mesmo lugar na mesma posição, as fotos podem ser bem parecidas, mas ainda tem seus direitos autorais garantido, afinal cada câmera possui seu conjunto de configurações particular.

- O trabalho deve ser fixado de forma tangível. Uma ideia é somente uma ideia...
- Para que um direito autoral exista precisa ser no mínimo criativo no que se propõe.
- Os direitos autorais não protegem a ideia, mas a demonstração física da ideia.
- Protegidos:
  - Documentação de software e manuais
  - Conteúdo de website
  - Imagens e ícones
  - Material de marketing
  - Apresentação de vendas
  - white papers
  - Material de treinamento
  - Anúncios
  - Fotografias
- Não protegidos:
  - Lista de regras
  - Lista de telefone
  - Receitas
  - Ideias
  - Fatos
  - Conceitos

O arquivo de copyright gerencia as seguintes permissões:

- Cópia
- Modificação
- Distribuição
- Execução - mais relevante para artes
- Mostragem - mais relevante para artes

> Se você encontrar um software na Internet para download e a licença não estiver presente, até que você encontre a licença, caso exista, você não tem nenhum direito de uso.

O tempo de duração de direito autorais é:

- Pessoa física 70 anos
- Corporativa 95 anos da primeira publicação ou 120 da criação

Depois deste tempo torna-se de domínio público, como o caso das obras de artes antigas. Qualquer um pode ter uma réplica em casa sem ferir nenhum direito.

## Domínio Público

![publicdomain](./pics/publicdomain.svg)
Um trabalho criativo pode se tornar de domínio público por `tempo` ou por vontade do dono.

- Nenhuma licença é requerida
- Geralmente é publicado alguma notícia sobre isso
- Qualquer um pode fazer cópias de trabalhos de domínio público.

É por isso que podemos ter uma réplica idêntica de uma obra de arte bem antiga.

## Licença proprietária

Uma licença de software proprietária é uma licença de software que concede ao titular dos direitos autorais a propriedade exclusiva do software e restringe seu uso, distribuição e modificação por outros indivíduos ou empresas. Essas licenças geralmente têm termos e condições mais restritivos do que as licenças de software livre ou de código aberto.

As licenças de software proprietárias podem incluir restrições como:

- Restrições de uso: as licenças podem permitir o uso do software apenas para determinados fins ou por um número limitado de usuários.
- Restrições de distribuição: as licenças podem impedir que o software seja distribuído sem a permissão do detentor dos direitos autorais.
- Restrições de modificação: as licenças podem impedir que o software seja modificado ou exigir que quaisquer modificações sejam aprovadas pelo detentor dos direitos autorais antes da distribuição.
- Restrições de cópia: as licenças podem impedir que o software seja copiado ou exigir que as cópias sejam marcadas como propriedade do detentor dos direitos autorais.
- Restrições de transferência: as licenças podem impedir que o software seja vendido ou transferido a terceiros.

As licenças proprietárias são comumente usadas por empresas que desejam manter o controle sobre o software que produzem e gerar receita por meio de sua distribuição. Essas licenças também são frequentemente usadas por empresas que têm segredos comerciais ou propriedade intelectual que precisam proteger.

Em resumo, uma licença de software proprietária é uma licença que concede ao titular dos direitos autorais a propriedade exclusiva do software e restringe seu uso, distribuição e modificação por outros indivíduos ou empresas. As licenças proprietárias geralmente têm termos e condições mais restritivos do que as licenças de software livre ou de código aberto.

## Trabalhos derivados

O trabalho derivado é uma criação de algo com base em outra coisa que possuí uma licença. Por exemplo, um filme baseado em um livro, um software que possui em seu código outros códigos de terceiros, etc.

> Qualquer trabalho que utilize algo de terceiro que não seja de domínio público é um trabalho derivado, inclusive pode ser utilizado várias criações de terceiros gerar uma novo trabalho.

No caso de softwares, tanto o uso de um código de terceiro quando de um binário geram trabalhos derivados.

E como fica a licença disso? Vários softwares open source estão licenciados sob diferentes termos, e como conviver com elas?
Para isso precisamos conhecer as principais licenças e quais as diferenças entre elas.

## Open Source vs Free Software

|  |  |  |
|-|-|-|
|![opensource](./pics/opensource.svg)| VS | ![copyleft](./pics/copyleft.svg)|

Um software é considerado open source quando utiliza uma licença do tipo open source.

Abaixo uma lista de licenças separadas em 2 grupos.

- Permissivas
  - As licenças que permitem que o software seja usado, modificado e distribuído sem restrições significativas ou obrigações de compartilhar o código fonte das modificações. Essas licenças são frequentemente escolhidas por desenvolvedores que desejam tornar seu software facilmente reutilizável em outros projetos, sem muitas restrições. No entanto, é importante lembrar que as licenças permissivas ainda possuem termos e condições específicas que precisam ser cumpridos, como a atribuição de direitos autorais e a isenção de garantias. É fundamental que os usuários leiam e entendam os termos de cada licença antes de usá-las em um projeto.
  >Deve ser preservado os direitos autorais do software e o proprietário original do software e este precisa ser atribuído e reconhecido em qualquer redistribuição do software para que este receba o crédito adequado pelo trabalho que foi realizado, além de proteger contra o uso indevido ou apropriação indevida do software.
  1. [MIT License](https://opensource.org/licenses/MIT)
  2. [BSD License](https://opensource.org/licenses/BSD-3-Clause)
  3. [Apache License](https://www.apache.org/licenses/LICENSE-2.0)
  4. [Creative Commons Licenses](https://creativecommons.org/licenses/)
  5. [zlib/libpng License](https://opensource.org/licenses/Zlib)
  6. [Boost Software License](https://www.boost.org/users/license.html)
- Restritivas (copyleft)
  - Essas licenças impõem mais restrições em como o software pode ser usado, modificado e distribuído, e geralmente exigem que as modificações feitas no software sejam disponibilizadas publicamente com a mesma licença.
  1. [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl-3.0.en.html)
  2. [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/)
  3. [Eclipse Public License (EPL)](https://www.eclipse.org/legal/epl-v10.html)
  4. [Affero General Public License (AGPL)](https://www.gnu.org/licenses/agpl-3.0.en.html)
  5. [Common Development and Distribution License (CDDL)](https://opensource.org/licenses/CDDL-1.0)
  6. [GNU Lesser General Public License (LGPL)](https://www.gnu.org/licenses/lgpl-3.0.en.html)
  7. [The Unlicense](https://unlicense.org/)
  8. [GNU Affero General Public License version 3](https://www.gnu.org/licenses/agpl-3.0.en.html)
  9. [GNU Lesser General Public License version 3](https://www.gnu.org/licenses/lgpl-3.0.en.html)

## Permissivas VS Restritivas

Geralmente as licenças permissivas são utilizadas para que o autor receba seu crédito, não financeiramente, mas moralmente e o proteja contra o uso indevido do seu trabalho ou isente-o da responsabilidade de algum dano que sua criação possa gerar. Por exemplo, se usarmos um código de terceiro e este criar algum bug que prejudique financeiramente quem está usando, o autor será inocentado. As licenças permissivas definem o grupo que defendem software `Open Source`.

Por outro lado, temos as licenças `Copyleft` que definem o grupo que defedem o `Software Livre (Free Software)`. A diferença está na licença.

Uma licença Copyleft é um tipo de licença de software livre que garante que as modificações e versões derivadas de um software também sejam licenciadas como software livre. Isso significa que qualquer pessoa que modifique ou use o software original para criar um novo trabalho também deve disponibilizar o novo trabalho como software livre sob os mesmos termos da licença original. É uma forma de proteger a liberdade do software e evitar que empresas ou indivíduos fechem o código do software, restringindo o acesso a outras pessoas. Ao garantir que o software permaneça livre e aberto, a licença Copyleft promove a colaboração e o compartilhamento, permitindo que outros construam e melhorem o software.

Porém, o parágrafo acima não é muito verdade, pois cada licença copyleft tem seus deveres, algumas são mais rígidas do que outras. Por isso é necessário aprender como conviver com vários software e várias licenças.

| Licença | Uso em projetos de código aberto | Código-fonte | Uso em projetos proprietários | Compatibilidade com outras licenças |
|---|---|---|---|---|
| GNU GPL | Requer que o software que utiliza o software licenciado também seja distribuído sob os termos da mesma licença | O código-fonte deve ser disponibilizado em todas as distribuições | O código-fonte deve ser disponibilizado junto com o software distribuído | Incompatível com algumas licenças de código aberto |
| GNU AGPL | Requer que o software que utiliza o software licenciado através de uma rede também seja distribuído sob os termos da mesma licença | O código-fonte deve ser disponibilizado em todas as distribuições | O código-fonte deve ser disponibilizado junto com o software distribuído | Incompatível com algumas licenças de código aberto |
| GNU LGPL \ Mozilla Public License (MPL)\ Eclipse Public License (EPL) Common Development and Distribution License (CDDL) | Permite que o software que utiliza o software licenciado seja distribuído sob qualquer licença, desde que as modificações feitas no software sejam disponibilizadas publicamente sob a mesma licença copyleft. | O código-fonte do software licenciado deve ser disponibilizado apenas se houver modificações | O código-fonte não precisa ser disponibilizado junto com o software distribuído | Compatível com algumas licenças de código aberto |
| Common Development and Distribution License (CDDL) | Permite que o software que utiliza o software licenciado seja distribuído sob qualquer licença, desde que as modificações feitas no software sejam disponibilizadas publicamente sob a mesma licença copyleft. | O código-fonte do software licenciado deve ser disponibilizado apenas se houver modificações | O código-fonte não precisa ser disponibilizado junto com o software distribuído | Incompatível com algumas licenças de código aberto |

>Tome cuidado no uso principalmente das bibliotecas GPL e AGPL para uso em software proprietário. O objetivo dessas licenças é torna o software livre, não somente open source.

![licenses](./pics/licenses.svg)

“Software livre” significa software que respeita a liberdade e a comunidade dos usuários. Isso não quer dizer que você possa fazer qualquer coisa, senão vira baderna.

## Danos

![damage](./pics/cmpatent.svg)

Não seguir corretamente o uso das licenças podem criar consequências graves para uma empresa ou pessoa.

- Milhões em danos morais e taxas
- Perda de propriedade intelectual
- Perda de produtividade, uma vez que irá interromper o ciclo de vida normal do software para ajustes.
- Perda de receita devido à interrupção judicial das distribuições de produtos
- Reputação prejudicada como um fornecedor de software confiável

## Como fazer corretamente?

O melhor caminho é ter um setor jurídico especilizado em direitos autorais. Não deve ser usado o que será mostrado abaixo nem como template, é somente um exemplo de como deveríamos referenciar uma licensa de terceiro.

Imaginemos que nossa solução de produto utiliza o Nginx que tem sua própria licensa. Como faríamos isso?

```txt
[Licença Proprietária]

Este software (o "Software") é propriedade exclusiva de [Nome da Empresa] e está protegido pelas leis de direitos autorais e outras leis de propriedade intelectual aplicáveis. O uso deste software está sujeito aos termos e condições estabelecidos nesta licença.

Concessão de licença:
a. A [Nome da Empresa] concede ao usuário uma licença pessoal, limitada, não exclusiva, intransferível e revogável para utilizar o Software e quaisquer materiais de documentação relacionados, apenas para fins internos do usuário e de acordo com os termos e condições desta licença.
b. O Software inclui o servidor web Nginx, que é licenciado sob a licença BSD de 2 cláusulas, cujos termos e condições são os seguintes:

"Copyright (c) [Ano] Nginx, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."

Ao utilizar o Nginx, o usuário concorda com os termos e condições da licença BSD de 2 cláusulas.

Restrições:
a. O usuário não pode sublicenciar, vender, alugar, arrendar, transferir ou conceder qualquer outra forma de direitos a terceiros.
b. O usuário não pode descompilar, desmontar, fazer engenharia reversa ou de outra forma tentar derivar o código fonte do Software, exceto na medida em que tal atividade seja expressamente permitida por lei.
c. O usuário não pode remover, obscurecer ou alterar qualquer aviso de direitos autorais, marcas registradas ou outras informações de propriedade incluídas no Software.

Garantia Limitada:
a. O Software é fornecido "como está" sem qualquer garantia, expressa ou implícita, incluindo, mas não se limitando a, garantias de adequação a uma finalidade específica, não violação ou garantias implícitas de comercialização ou adequação para uso comercial.
b. A [Nome da Empresa] não garante que o Software atenderá aos requisitos do usuário ou que o uso do Software será ininterrupto, oportuno, seguro ou livre de erros.

Limitação de Responsabilidade:
a. Em nenhuma circunstância, a [Nome da Empresa] será responsável por quaisquer danos diretos,]
```
