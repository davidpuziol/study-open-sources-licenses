---
sidebar_position: 1
id: Propriedade Intelectual
title: Propriedade Intelectual
description: ""
---

# Intelectual Properties

![ip](./pics/Intellectual_property_Noun_project.svg)

A história da propriedade intelectual remonta ao século XVIII, com a Lei de Direitos Autorais de 1710, também conhecida como Estatuto da Rainha Ana, que foi a primeira lei a conceder aos autores o direito de controlar a cópia de suas obras por um período de tempo limitado.

A Lei de Direitos Autorais dos Estados Unidos, criada em 1790, foi uma das primeiras leis de direitos autorais modernas, concedendo aos autores o direito exclusivo de publicar, distribuir e vender suas obras por um período de 14 anos, com a opção de renovação por mais 14 anos.

Ao longo dos anos, as leis de direitos autorais evoluíram para abranger outras formas de mídia, como música, cinema e software. As leis de direitos autorais foram criadas para proteger os criadores de obras intelectuais e incentivar a inovação.

## O que é propriedade intelectual?

A propriedade intelectual é uma forma importante de proteger a inovação e incentivar o desenvolvimento de novas ideias e produtos, permitindo que as pessoas ou empresas que criam ou desenvolvem essas ideias possam controlar sua utilização e comercialização. Quem iria criar algo se não tivesse direito sobre isso?

É um termo utilizado para descrever os direitos de propriedade que as pessoas ou empresas têm sobre suas criações intelectuais. Essas criações incluem patentes, direitos autorais, marcas registradas, segredos comerciais e outros direitos relacionados à propriedade intelectual.

> É uma proriedade de algo que não existe fisicamente, que não pode ser tocada ou sentida, mas ainda existe.

Existem 4 diferentes sistemas para proteger o direito de propriedade intelectual

- Copyright (Direitos Autorais)
  - É um tipo de propriedade intelectual que protege obras  originais de autoria, como livros, músicas, filmes, obras de arte, software, entre outras. O copyright é concedido automaticamente ao autor ou criador da obra no momento em que ela é criada, e geralmente dura por um período determinado de tempo, após o qual a obra passa a ser de domínio público.
  Os direitos autorais conferem ao criador da obra o direito exclusivo de reproduzir, distribuir, exibir, executar e criar obras derivadas dessa obra original. Isso significa que, se alguém quiser usar a obra de outra pessoa, é necessário obter permissão do autor ou pagar por essa utilização.
- Patente
  - Esse tipo de propriedade intelectual que protege uma invenção ou descoberta útil e nova por um determinado tempo. As patentes são concedidas pelos governos aos inventores ou titulares dos direitos, e oferecem a esses indivíduos ou empresas o direito exclusivo de produzir, vender ou usar a invenção. As patentes também ajudam a garantir que a invenção não seja copiada ou usada sem autorização, o que pode prejudicar o negócio do inventor. No entanto, a obtenção de uma patente pode ser um processo complexo e demorado, que envolve apresentação de documentos detalhados e análise por parte de um escritório de patentes. Além disso, as patentes podem expirar após um determinado período, permitindo que outros usem a tecnologia sem o consentimento do inventor.
- Trademarks (Marca Registrada)
  - Propriedade intelectual que protege um símbolo, logotipo, palavra ou frase usados para identificar e distinguir produtos ou serviços de uma empresa de outras empresas. As marcas registradas são concedidas pelos governos aos proprietários das marcas e oferecem a esses indivíduos ou empresas, o direito exclusivo de usar a marca registrada em conexão com seus produtos ou serviços.
  As marcas registradas são importantes porque permitem que as empresas construam e protejam sua identidade de marca. Uma marca registrada ajuda os consumidores a identificar produtos ou serviços específicos com uma determinada empresa, e também protege as empresas contra o uso indevido de sua marca por outras empresas.
- Trade Secret (Segredos Comerciais)
  - São informações confidenciais usadas por uma empresa para se diferenciar da concorrência ou ter uma vantagem competitiva no mercado. Essas informações podem incluir fórmulas, processos, designs, listas de clientes, planos de negócios e outros segredos empresariais.
  Ao contrário das patentes ou marcas registradas, não há um processo formal de registro para proteger os segredos comerciais. Em vez disso, as empresas dependem de medidas internas de segurança para proteger essas informações e impedir o acesso não autorizado por parte de funcionários, concorrentes ou outras partes interessadas.

Propriedade intelecutal podem ser:

- Vendidas: Quando o dono da propriedade perde todo o direito sobre ela e transfere para outra pessoa os direito a partir de um retorno financeiro.
- Licenciadas: Quando existe um documento que protege o que pode ser feito com o uso do objeto em questão. Muito utilizadas por software inclusise. No caso alguém utilizar o objeto diferentemente do que a licença permite, pode sofrer consequências jurídicas.

1. As patentes são usadas para proteger invenções ou descobertas úteis e novas
2. Os direitos autorais protegem obras originais de autoria, como livros, músicas, filmes e outras expressões criativas.
3. As marcas registradas protegem marcas comerciais e logotipos usados em conexão com produtos ou serviços, enquanto os segredos comerciais protegem informações confidenciais de negócios, como fórmulas e processos exclusivos.

## Third Party IP

É quando usa-se algo que não é sua propriedade intelectual, mas a licença permite que o uso, desde que respeite os termos da licensa. Não se destina somente a software, mas também a trademarks, documentos, imagens, etc.

---

>A partir deste momento e de um overview sobre propriedade intelectual, o estudo será direcionado com o foco de direitos autorais (copyright). Se alguém quiser completar o estudo, fique a vontade para contribuir.
