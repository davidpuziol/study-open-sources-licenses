# Controle de licenças

O processo de criar uma licença para uma release do produto é um processo colaborativo na empresa. O setor jurídico precisa das informações corretas para criar uma licença de um software/software-release, e quem pode ajudar eles? Os responsáveis pelo desenvolvimento da solução.

## Agile vs Licenças

Uma das ferramentas mais utilizadas no processo de desenvolvimento de software é a metodologia ágil. Como poderíamos incluir o controle das licenças na entrega do produto em produção?

No processo de desenvolvimento, várias bibliotecas e códigos de terceiros são usados no caminho. Como garantir que esses produtos de terceiros não irão impactar na licença do produto final?

Para resolver esse problema, é fundamental desenvolver uma estratégia adaptada às complexidades da situação. Infelizmente, não há uma solução definitiva que possa ser aplicada em todos os casos. É essencial, no entanto, garantir que as informações sobre os produtos de terceiros envolvidos no projeto cheguem ao setor responsável pelo empacotamento final da solução a tempo de gerar a licença do produto final para a produção.

Itens importante que podem ajudar não se perder no processo.

- Crie um treinamento de propriedade intelectual para toda a empresa.

- Tenha uma estratégia para que os colaboradores sejam capazes divulgar o que esta usando.

- É necessário um setor responsável para avaliar se a licença de terceiros impactará no software ou tem alguma incompatibilidade com alguma outra licença.

- Uma boa estratégia é ter um formulário, para ser preenchido pelo colaborador, com todos os dados necessários para um inventário de propriedades intelectuais de terceiros. Isso facilitará o setor jurídico a gerar a licença das releases e manterá um histórico do produto.

- No caso do setor jurídico, um documento em anexo com as licenças poderia ser utilizado, sendo que este documento viria do formulário preenchido, evitando assim mexer o tempo inteiro no documento de licença.

- Gere um documento assinado digitalmente pelos mantenedores da solução que os forcem a preencher os formulários corretamente e anexe junto as licenças finais.

- Tenha um banco de dados só para guardar as informações vindas dos colaboradores.

- Não permita uso de licenças de software livre exceto se seja extremamente necessário, pois pode impactar em todo o projeto.

- Crie uma estratégia de review antes do lançamento das releases.

![disclosure](./pics/discloruse.svg)
